#!/bin/bash

#first step install curl/wget
echo "Download depencies.."
apt update
apt install -y curl \
        wget \
        python3-pip \ 
        vim

#Datadog installtion
if [ -f /usr/bin/datadog-agent ]
then
    echo "datadog already installed, checking if running.."
    status=$(service datadog-agent status | grep "is running")
     if [ -f opt/datadog-agent/run/agent.pid ]
     then
        echo "Datadog is running!"
    else
       echo " Starting datadog agent process"
       service datadog-agent start
    fi
else
     echo " Datadog not found.. installing.."
     wget -O - https://s3.amazonaws.com/dd-agent/scripts/install_script.sh | bash

fi

#Databricks cli
if [ -f /usr/local/bin/databricks ] 
then
    echo "Databricks CLI installed!"
else
    pip3 install databricks-cli
    pip3 install click
fi
echo " configure databricks environment.."
    export LC_ALL=C.UTF-8
    export LANG=C.UTF-8
if [ -f ~/.databrickscfg ] 
then
    echo "environment already installed"
else
    echo "[DEFAULT]
    host = $DATABRICKS_HOST
    token = $TOKEN
    jobs-api-version = 2.0" >> ~/.databrickscfg 
    echo "Configured DEFAULT profile"
fi
echo "checking for running jobs.."
running=$(databricks clusters list | awk '/RUNNING/{++cnt} END {print cnt}' )
export running
if [ $running -ge 1 ]
then   
    echo "number of running jobs : ${running} "
else
    echo " NO JOBS ARE RUNNING!!!!"
fi

# create checkvaule.py in check.d dir in datadog
echo "creating checkvaule.py .."
echo "
from checks import AgentCheck
import os

class DataBricksCheck(AgentCheck):
  def check(self, instance):
    jobs_running=(os.environ["running"])
    self.gauge("databricks.jobs.running", jobs_running)
" >> /etc/datadog-agent/checks.d/checkvalue.py
echo "creating checkvaule.yaml file.."
echo "
init_config:
  
instances:
  [{}]
" >> /etc/datadog-agent/conf.d/checkvalue.yaml

echo "restarting datadog service.."
service datadog-agent restart
sleep 3
service datadog-agent restart